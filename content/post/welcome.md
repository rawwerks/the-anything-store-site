+++
title = "Welcome to The Anything Store"
date = "2019-01-01"
draft = false
author = "Raymond Weitekamp"
+++

Welcome to The Anything Store.

The Anything Store is a matter-renderer that rapidly manifests physical products into existence. It is an API to the physical world, where chemistry and code commute.   

The Anything Store is manifested by a network, not a single entity. It is powered by direct digital manufacturing – form and function in one step.   

The Anything Store will make hardware development tomorrow look like software development today. It will enable distributed, on-demand, inventory-free manufacturing that will dramatically reduce the cost and carbon footprint of durable goods. It will unlock new business models, featuring parametric product design and mass customization. 

The Anything Store will democratize access to advanced manufacturing, unleashing a new creative class of designers, inventors and engineers.

The Anything Store is a shared vision for a connected physical world. What does it mean to you?

*hello [at] anythingstore*